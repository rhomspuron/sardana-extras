# How to register a plugin project in the catalogue?

The registration process is executed via GitLub [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/).

These are the typical steps of such a process:

1. Select the appropriate category for your project.
2. Look if in the selected category table exists an entry for the given subject e.g. Tango entry in the plugin/software category table.
    * If there is already an entry, add your plugin as a next row
    * If there is no entry, create a new row respecting the alphabetical order 
      within the category table.
3. In the row, fill the following information: name, description, link(s) to 
project(s)
4. Create a MR with the changes explained in the point 3.

Note: In order to facilitate browsing of the plugin/system category it is splitted in sub-tables per facility.
Please add your project to an already existing facility sub-table, or create a new one for your facility.
